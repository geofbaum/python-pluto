import math
from pint import _DEFAULT_REGISTRY
ureg = _DEFAULT_REGISTRY
# The following functions are Python built-in and can be used without import:
# abs(), ..., max(), min(), ...

# The following functions have not been implemented yet:
# acosec(), acosec2(), ...


def abs(value):
    return __builtins__.abs(value.magnitude) * value.units


def _reciprocal(value):
    return 1.0/value


def acos(value):
    return math.acos(value.magnitude) * ureg.radian


def acosec(value):
    return math.asin(_reciprocal(value.magnitude)) * ureg.radian


def acosec2(value_1, value_2):
    value = value_1 / value_2
    return math.asin(_reciprocal(value.magnitude)) * ureg.radian


def acotan(value):
    return ((math.pi/2) - math.atan(value.magnitude)) * ureg.radian


def acotan2(value_1, value_2):
    return ((math.pi/2) - math.atan2(value_1.magnitude, value_2.magnitude)) *\
        ureg.radian


def asec(value):
    return math.acos(_reciprocal(value.magnitude)) * ureg.radian


def asec2(value_1, value_2):
    value = value_1 / value_2
    return math.acos(_reciprocal(value.magnitude)) * ureg.radian


def asin(value):
    return math.asin(value.magnitude) * ureg.radian


def atan(value):
    return math.atan(value.magnitude) * ureg.radian


def atan2(value_1, value_2):
    return math.atan2(value_1.magnitude, value_2.magnitude) * ureg.radian


def average(*value):
    return sum(value)/len(value)


def ceiling(value):
    return math.ceil(value.magnitude) * value.units


def cos(value):
    return math.cos(value.m_as('rad')) * ureg.dimensionless


def cosec(value):
    return _reciprocal(math.sin(value.m_as('rad'))) * ureg.dimensionless


def cosh(value):
    return math.cosh(value.m_as('rad')) * ureg.dimensionless


def cotan(value):
    return _reciprocal(math.tan(value.m_as('rad'))) * ureg.dimensionless


def floor(value):
    return math.floor(value.magnitude) * value.units


def ln(value):
    return math.log(value.magnitude) * value.units


def log(value):
    return math.log10(value.magnitude) * value.units


def max(value_1, value_2):
    value_2.ito(value_1.units)
    return __builtins__.max(
        value_1.magnitude, value_2.magnitude) * value_1.units


def min(value_1, value_2):
    value_2.ito(value_1.units)
    return __builtins__.min(
        value_1.magnitude, value_2.magnitude) * value_1.units


def quotient(value_1, value_2):
    value_2.ito(value_1.units)
    return (value_1.magnitude // value_2.magnitude) * value_1.units


def remainder(value_1, value_2):
    value_2.ito(value_1.units)
    return math.remainder(value_1.magnitude, value_2.magnitude) * value_1.units


def round(value, places=0):
    return __builtins__.round(value.magnitude, places) * value.units


def sec(value):
    return _reciprocal(math.cos(value.m_as('rad'))) * ureg.dimensionless


def sin(value):
    return math.sin(value.m_as('rad')) * ureg.dimensionless


def sinh(value):
    return math.sinh(value.m_as('rad')) * ureg.dimensionless


def sqrt(value):
    return math.sqrt(value.magnitude) * value.units


def tan(value):
    return math.tan(value.m_as('rad')) * ureg.dimensionless


def tanh(value):
    return math.tanh(value.m_as('rad')) * ureg.dimensionless


def truncate(value):
    return math.trunc(value.magnitude) * value.units


def pi():
    return math.pi * ureg.dimensionless


def e():
    return math.e * ureg.dimensionless


def G():
    _G_const = ureg.newtonian_constant_of_gravitation
    return _G_const.to('meter**3 / kilogram / second**2')
