import pathlib
import sys
import os
import time
import logging

from .parser import PlutoParser
from .transformer import PlutoTransformer
from .language.enums import ExecutionState, ConfirmationStatus


def pluto_to_python(pluto_string, procedure_name='anon', debug=False):
    """Convert a string containing a PLUTO procedure into Python source.

    Having this as a separate method enables doing this without
    touching the file system.

    The procedure_name is needed to set the name of the generated
    Python class.
    If debug is True, return the transformed output and the tree.
    """
    tree = PlutoParser.parse(pluto_string)
    transformer = PlutoTransformer(procedure_name=procedure_name)
    transformed = transformer.transform(tree)
    if not debug:
        return transformed
    else:
        return transformed, tree


def pluto_tree(pluto_file=None):
    """Print the parse tree.
    """
    if pluto_file is None:
        if len(sys.argv) != 2:
            raise Exception("Please provide .pluto file as an argument")
        pluto_file = sys.argv[1]
    with open(pluto_file) as f:
        contents = f.read()
        tree = PlutoParser.parse(contents)
        print(tree.pretty())


def pluto_convert(pluto_file=None):
    """Convert a .pluto file into a Python file at the same location.

    The file location can be passed as an argument to the function or
    as an argument to the registered console_script entrypoint.
    """
    if pluto_file is None:
        if len(sys.argv) != 2:
            raise Exception("Please provide .pluto file as an argument")
        pluto_file = sys.argv[1]

    with open(pluto_file) as f:
        contents = f.read()
    pluto_file_path = pathlib.Path(pluto_file)
    proc_name = pluto_file_path.stem
    python_source = pluto_to_python(contents, procedure_name=proc_name)
    py_filename = pluto_file_path.with_suffix('.py')
    with open(py_filename, 'w') as py:
        py.write(python_source)


def pluto_run(file=None, debug=False):
    """Run a pluto file.

    The file location can be passed as an argument to the function or
    as an argument to the registered console_script entrypoint.
    """
    if file is None:
        if len(sys.argv) != 2:
            raise Exception("Please provide a .pluto file as argument")
        file = sys.argv[1]

    folder, filename = os.path.split(file)
    procedure_name = os.path.splitext(filename)[0]

    with open(file, 'r') as f:
        pluto_text = f.read()

    py_code = pluto_to_python(pluto_text, procedure_name)
    sys.path.append(os.path.abspath(folder))
    exec(py_code, locals())
    Procedure_ = locals()["Procedure_" + procedure_name]
    procedure = Procedure_()

    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    print("Running procedure [Procedure_{}]".format(filename))
    print(80 * "-")
    procedure.run()
    while procedure.execution_state != ExecutionState.COMPLETED:
        time.sleep(0)
    print(80 * "-")
    if procedure.confirmation_status == ConfirmationStatus.CONFIRMED:
        print("Procedure completed successfully.")
    elif procedure.confirmation_status == ConfirmationStatus.ABORTED:
        print("Procedure was aborted!")
    else:
        print("Procedure confirmation error!")
