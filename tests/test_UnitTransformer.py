import pluto
from pluto.transformer import UnitsTransformer


def test_units_transformer(unit_example):
    """All unit examples parse back to their strings."""
    tree = pluto.parser.EngineeringUnitsParser.parse(unit_example)
    assert UnitsTransformer().transform(tree) == unit_example
