import pytest
from lark import Lark

import pluto
import pluto.grammar


@pytest.mark.parametrize(
    'parser', ['earley', pytest.param('lalr', marks=pytest.mark.xfail)])
def test_pluto_grammar_loads(parser):
    with open(pluto.grammar.pluto_grammar_file) as plt:
        _ = Lark(
            plt,
            start='procedure_definition',
            parser=parser,
            debug=False)


@pytest.mark.parametrize('parser', ['earley', 'lalr'])
def test_engineering_unit_grammar_loads(parser):
    with open(pluto.grammar.engineering_units_grammar_file) as grm:
        _ = Lark(
            grm,
            start='engineering_units',
            parser=parser,
            debug=True)


def test_eng_units(unit_example, eng_parser, request):
    """Test if unit strings get parsed"""
    # FIXME: Find the problem in the grammar that makes these cases fail
    failing_tests = [
        'lalr-mm',
        'lalr-dB',
        'lalr-min',
        'lalr-AU',
        'lalr-pc',
    ]
    if request.node.name.split('[')[-1].split(']')[0] in failing_tests:
        pytest.xfail('Some unit strings are not parsed in lalr')

    eng_parser.parse(unit_example)


@pytest.mark.skip
def test_unit_tree(eng_parser):
    """TODO: Test if unit strings get parsed correctly.

    Use some difficult examples like 'mm' to check if prefixes/units get
    recognized correctly."""
    pass


# TODO: Add test of PLUTO examples
