from setuptools import setup, find_packages

setup(
    name='python-pluto',
    version='1.0.0',
    description='Parse PLUTO scripts to Python',
    license='MIT',
    python_requires='>=3',
    keywords='ecss pluto dsl',
    packages=find_packages(exclude=['examples']),
    package_data={'': ['*.ebnf', '*.lark']},
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'pluto_convert=pluto:pluto_convert',
            'pluto_run=pluto:pluto_run',
            'pluto_tree=pluto:pluto_tree'
        ],
    },
    install_requires=['lark-parser', 'pint'],
    extras_require={
        'test': ['pytest', 'coverage', 'pytest-cov'],
    },
)
