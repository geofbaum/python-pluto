# Python-Pluto
`python-pluto` is a Python module for converting PLUTO scripts into native Python code.

PLUTO stands for *Procedure Language for Users in Test and Operations* and is a domain specific language (DSL). It was developed by the European Cooperation for Space Standardization (https://ecss.nl) for the automation of procedures in the development and utilization of space systems (but not limited to it). The PLUTO language is defined in standard [ECSS-E-ST-70-32C](docs/ECSS-E-ST-70-32C31July2008.pdf).

The advantage of using a DSL rather than general purpose programming language is that the syntax is typically easier to learn, less prone to errors, and focuses on the matter of the domain. In the case of PLUTO, this is the monitoring and control of systems, such as satellites, drones, test facilities, etc.

However, one needs to parse the DSL scripts into an executable code in order to run it. This is what the `python-pluto` package is for.

## Getting Started
Install the `python-pluto` module:

```bash
pip install git+https://gitlab.com/librecube/lib/python-pluto.git
```

Write a PLUTO script, for example:

```
/* example.pluto */
procedure
  initiate and confirm step INIT
    initiate and confirm Activate of PowerSupply;
    wait for 2s;
    confirmation
      if Mode of PowerSupply = "ON"
    end confirmation
  end step;
end procedure
```

Save this file as `example.pluto`.

> To see how the generated Python code looks for this example, run
`pluto_convert example.pluto`, which will create an example.py file. This step
is however not needed and only for information or debugging.

To execute the PLUTO script, a simple command line tool is available:

```bash
pluto_run example.py
```

> Note that you can write your own automation system to execute and monitor
procedures. This is described in the [User Guide](docs/users.md).

However, it will not run yet. You first need to create a `model.py` file that defines the objects `Activate`, `PowerSupply`, and `Mode` that are referenced in the code. The purpose of the model file is to define a model of your system, namely what and how commands are being sent to the system under control and what reporting data is available from it. To allow this in a generic way, ECSS developed the so-called Space System Model (SSM) in [ECSS-E-ST-70-31C](docs/ECSS-E-ST-70-31C31July2008.pdf) that essentially defines System Elements, Activities, Reporting Data, and Events for establishing such a system model. In a nutshell, a system is composed of a tree of child system elements, where each element can have activities, events, and reporting data associated with it.

An example `model.py` file could look like this:

```python
# model.py
from pluto.system_model import SystemElement, Activity, ReportingData

PowerSupply = SystemElement("PowerSupply")

mode = "OFF"

def func(arguments, directives):
    global mode
    mode = "ON"

act = Activity("Activate")
act.run = func
PowerSupply.add_activity(act)

rep = ReportingData("Mode")
rep.get_value = lambda: mode
PowerSupply.add_reporting_data(rep)
```

What we are doing here is to define the root system element `PowerSupply` and attach an `Activity` object and a `ReportingData` object to it. The activity has a `run` method that we must define, in this case it sets the global mode variable to `ON`. In a real case this might be a TCP/IP request to a remote machine, for example. Similar, the `get_value` method of the reporting data returns the mode variable.

To learn more about how to write PLUTO procedures and how to define your system model, head to the [User Guide](docs/users.md).

If you would like to help adding missing language features or modify the parser to you likening, then please have a look at the [Developer Guide](docs/developers.md).
