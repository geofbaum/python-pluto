from pluto.system_model import SystemElement, Activity, ReportingData


DummySatellite = SystemElement("DummySatellite")

AOCS = SystemElement("AOCS")
DummySatellite.add_child_element(AOCS)


def func(arguments, directives):
    print("Activity called with {}, {}".format(arguments, directives))


act = Activity("DummyCommand")
act.run = func
DummySatellite.add_activity(act)


Mode = "ON"


def mode_on(arguments, directives):
    global Mode
    Mode = "ON"


def mode_off(arguments, directives):
    global Mode
    Mode = "OFF"


act = Activity("SwitchON")
act.run = mode_on
AOCS.add_activity(act)


act = Activity("SwitchOFF")
act.run = mode_off
AOCS.add_activity(act)


rep = ReportingData("Mode")
rep.get_value = lambda: Mode
AOCS.add_reporting_data(rep)
