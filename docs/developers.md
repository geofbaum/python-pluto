# Developer Guide

Welcome to the developer guide, where you can learn how to actively contribute
to this project, or adjust the code to your own needs. All your support is
highly appreciated!

## Installation

A default development environment on a Linux machine can be set up as follows,
using a Python virtual environment:

```
$ git clone https://gitlab.com/librecube/lib/python-pluto.git
$ git checkout develop
$ virtualenv venv
$ source venv\bin\activate
$ pip install -e .[test]
```

## Workflow

To contribute to this project by fixing bugs or adding new features, please
follows these steps. Note that the `master` branch of the repository is
where stable, production ready code lives and the `develop` branch contains
new bug fixes and features, not yet released to the master.

> You may use any code editor of your choice. We recommend [Atom](https://atom.io/).

### Update your local repository

Any contribution starts off from the `develop` branch. Hence make sure that
your local `develop` branch is up-to-date with the remote `develop` branch:

```
$ git fetch origin
$ git pull origin
```

### Decide what to work on

First have a look at the [issues](https://gitlab.com/librecube/lib/python-pluto/issues)
to find bugs and features that you may work on. You may also use the
[librecube-pluto channel](https://riot.im/app/#/room/!DYAKaIbRbFKLWmKDNn:matrix.org) to
discuss with other developers.

### Create a dedicated branch

Branch off from `develop` by creating your dedicated branch for implementing your
changes. Name your branch as `#xx-yyyy` where xx is the number of the issue and
yyyy is a descriptive name or short description. For example:

```
$ git branch #41-update-readme
$ git checkout #41-update-readme
```

### Work on your branch

Apply your changes to the code and commit often. Make sure to push to your branch
on the remote to help see others your progress.

**Important**: Before pushing to the remote, make sure to run the tests:

```
$ pytest
```

### Request merge into develop branch

When you are done with your work, issue a merge request so that your changes
are merged into the `develop` branch, after being reviewed.
